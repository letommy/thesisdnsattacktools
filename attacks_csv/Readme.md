# Generate Attacks From CSV
---
These scripts are used to generate a CSV file for DNS attacks and replay them from the CSV file.

## Generate DNS Query Scan CSV
---
To generate a CSV file for a DNS query scan:

`./dns_scan_csv.py [-h] [-r N] [-p N] [-i N] OUTPUT_CSV_FILE DOMAINS_FILE RATE`

|Positional Argument|Info|
|---|---|
|OUTPUT_CSV_FILE	|name of the output CSV file|
|DOMAINS_FILE		|file containing the list of domains to query|
|RATE				|rate in packets per hour|

|Optional Argument|Info|
|---|---|
|-h, --help				|show a help message and exit  |
|-r N, --repeat N		|number of repeated queries to each IP address|
|-p N, --pattern N		|pattern of repeated queries: repeat immediately (0) or finish block then repeat query (1)|
|-i N, --increment N	|scanning pattern|

e.g. `./dns_scan_csv.py dns_scan_r60_d5_n5_p0_i5.csv d5.txt 60 -r 5 -p 0 -i 5`

where:

- 'dns_scan_r60_d5_n5_p0_i5.csv' is the name of the output CSV file which contains the entries/packets for the DNS query scan
- 'd5.txt' is the name of the TXT file which contains the list of domains to query
- '60' is the rate in packets per hour at which the scanner is sending queries 
- '-r 5' is the number of repeated queries to each IP address
- '-p 0' indicates that repeated queries are sent immediately/consecutively, i.e. x.x.x.0, x.x.x.0, ..., x.x.x.0, x.x.x.1, ...
- '-i 5' is the increment interval, i.e. x.x.x.0, x.x.x.5, x.x.x.10, ..., x.x.x.255, x.x.x.1, x.x.x.6, ...

To generate the CSV files automatically:

`./auto_dns_scan_csv.sh`

Modify the parameters in the arrays in the auto_dns_scan_csv.sh file accordingly.

## Generate DNS Query Flood CSV
---
To generate a CSV file for a DNS query flood:

`./dns_query_flood.py OUTPUT_CSV_FILE TARGET DOMAINS_FILE RATE`

|Positional Argument|Info|
|---|---|
|OUTPUT_CSV_FILE	|name of the output CSV file|
|TARGET				|target DNS server|
|DOMAINS_FILE		|file containing the list of domains to query|
|RATE				|rate in packets per hour|

e.g. `./dns_flood_csv.py dns_flood_r60_d5.csv 10.0.0.43 d5.txt 60`

where:

- 'dns_flood_r60_d5.csv' is the name of the output CSV file which contains the entries/packets for the DNS query flood
- '10.0.0.43' is the target DNS server
- 'd5.txt' is the name of the TXT file which contains the list of domains to query
- '60' is the rate in packets per hour at which the flooder is sending queries

To generate the CSV files automatically:

`./auto_dns_flood_csv.sh`

Modify the parameters in the arrays in the auto_dns_flood_csv.sh file accordingly.

## Generate Attack
---
To generate the attack from a CSV file:

`sudo ./play_csv.py CSV_FILE`

## Automate Attacks
---
To automate the attacks:

`sudo ./auto_attack.sh >> log.txt`

The log.txt file contains a description of each attack as well as their start and end times.