#!/usr/bin/python

import sys
import re
import time
import csv
from scapy.all import *
import socket
from pinject import IP, UDP

DNS_PORT = 53;

DNS_PAYLOAD = '\x00\x01\x00\x00\x00\x00\x00\x01' \
              '{}\x00\x00\x01\x00\x01\x00\x00\x29\x10\x00' \
              '\x00\x00\x00\x00\x00\x00'

qry_dict = {'true': '\x00\x00', 'false': '\x80\x00'}
opcode_dict = {'StandardQuery': '\x00\x00', 'InverseQuery': '\x08\x00', 'Status': '\x10\x00'}
truncated_dict = {'0': '\x00\x00', '1': '\x02\x00'}
recursion_desired_dict = {'0': '\x00\x00', '1': '\x01\x00'}

def flood(t, dst, src, qry_id, qry_rsp, opcode, authoritative, truncated, recursion_desired, recursion_available, domain):
    if not isIPv4(src) or not isIPv4(dst):
        print("Address isn't valid, skipping...")
        return
    dst = re.sub(r'^\d+', '10', dst)
    domain = re.sub(r'^\\', '', domain)
    domain = re.sub(r'\|\\$', '', domain)
    sock = socket.socket(socket.AF_INET, socket.SOCK_RAW, socket.IPPROTO_RAW)
    try:
        flags = '\x00\x00'
        payload = generate_payload(flags, qry_rsp, opcode, truncated, recursion_desired)
        query = get_dns_query(qry_id, domain, payload)
        send(sock, src, dst, query)
    except KeyboardInterrupt:
        print("\nInterrupted")
    sock.close()
    return

def isIPv4(addr):
    try:
        socket.inet_aton(addr)
        return True
    except socket.error:
        return False

def generate_payload(flags, qry_rsp, opcode, truncated, recursion_desired):
    flags = xor(flags, qry_dict[qry_rsp])
    flags = xor(flags, opcode_dict[opcode])
    flags = xor(flags, truncated_dict[truncated])
    flags = xor(flags, recursion_desired_dict[recursion_desired])
    payload = '{}' + flags + DNS_PAYLOAD
    return payload

def xor(flags, mask):
    i = 0
    result = ""
    for c in flags:
        result += chr(ord(c) ^ ord(mask[i]))
        i += 1
    return result

def get_dns_query(qry_id, domain, payload):
    id = struct.pack('H', qry_id)
    qname = get_qname(domain)
    return payload.format(id, qname)

def get_qname(domain):
    labels = domain.split(".")
    qname = ""
    for label in labels:
        if len(label):
            qname += struct.pack('B', len(label)) + label
    return qname

def send(sock, src, dst, payload):
    dst_port = RandShort()
    udp = UDP(dst_port, DNS_PORT, payload).pack(src, dst)
    ip = IP(src, dst, udp, proto=socket.IPPROTO_UDP).pack()
    sock.sendto(ip + udp + payload, (dst, DNS_PORT))

def main():
    with open(sys.argv[1], "rb") as csvfile:
        reader = csv.reader(csvfile)
        start_time = None
        prev_time = None
        prev_row = []
        next_row = []
        while True:
            chunk = []
            if next_row:
                chunk.append(next_row)
                next_row = []
            for row in reader:
                t = int(row[1])
                if start_time == None:
                    start_time = t
                if t < start_time + 3:
                    chunk.append(row)
                else:
                    next_row = row
                    break
            pps = {}
            for row in chunk:
                t = int(row[1])
                if t not in pps:
                    pps[t] = 1
                else:
                    pps[t] += 1
            print(time.strftime("%Y-%m-%d %H:%M:%S"))
            print(pps)
            if prev_time == None:
                prev_time = int(chunk[0][1])
            prev_epoch = time.time()
            for i in range(len(chunk)):
                t = int(chunk[i][1])
                interval = t - prev_time
                print(interval)
                while time.time() < prev_epoch + interval:
                    pass
                dst = chunk[i][0]
                src = chunk[i][2]
                qry_id = int(chunk[i][3])
                qry_rsp = chunk[i][4]
                opcode = chunk[i][5]
                authoritative = chunk[i][6]
                truncated = chunk[i][7]
                recursion_desired = chunk[i][8]
                recursion_available = chunk[i][9]
                domain = chunk[i][10]
                flood(t, dst, src, qry_id, qry_rsp, opcode, authoritative, truncated, recursion_desired, recursion_available, domain)
                prev_time = t
                prev_epoch = time.time()
                time.sleep(1.0 / pps[t] * 0.75)
            start_time = t + 1
            if row == prev_row:
                break
            prev_row = row
    print(time.strftime("%Y-%m-%d %H:%M:%S"))

if __name__ == "__main__":
    main()
