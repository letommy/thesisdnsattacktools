#!/usr/bin/python

import argparse
import random
import collections

HELP = (
    "csv file to write to",
    "txt file of domains to query",
    "number of queries per hour",
    "number of repeated queries to each IP address",
    "pattern of repeated queries: repeat query immediately (0) OR finish block then repeat query (1)",
    "scanning pattern"
)

OPTIONS = (
    (('-r', '--repeat'), dict(dest='repeat', metavar='N', type=int, default=1, help=HELP[3])),
    (('-p', '--pattern'), dict(dest='pattern', metavar='N', type=int, default=0, help=HELP[4])),
    (('-i', '--increment'), dict(dest='increment', metavar='N', type=int, default=1, help=HELP[5]))
)

class MyOrderedDict(collections.OrderedDict):
    def next_key(self, key):
        next = self._OrderedDict__map[key][1]
        if next is self._OrderedDict__root:
            for key in self:
                return key
        return next[2]

prefices = MyOrderedDict([("129.94", 0), ("131.236", 0), ("149.171", 0), ("169:254", 0), ("192.168", 0), ("203.10", 0)])

def generate_IP(prefix, block_3, block_4, repeat, r):
    if block_4 >= 255 and (r >= repeat or repeat == 1):
        block_3 += 1
        block_4 = 0
        r = 1
        if block_3 > 255:
            prefices[prefix] += 1
            prefix = prefices.next_key(prefix)
            block_3 = 0
    else:
        if r < repeat and block_4 >= 0:
            r += 1
        else:
            block_4 += 1
            r = 1
    ip = ".".join([prefix, str(block_3), str(block_4)])
    return ip, prefix, block_3, block_4, r

def generate_IP_2(prefix, block_3, block_4, repeat, r):
    if block_4 >= 255:
        block_4 = 0
        if r < repeat - 1:
            r += 1
        else:
            block_3 += 1
            block_4 = 0
            r = 1
            if block_3 > 255:
                prefices[prefix] += 1
                prefix = prefices.next_key(prefix)
                block_3 = 0
    else:
        block_4 += 1
    ip = ".".join([prefix, str(block_3), str(block_4)])
    return ip, prefix, block_3, block_4, r

def generate_IP_increment(prefix, block_3, block_4, increment, repeat, r):
    if block_4 + increment > 255 and (r >= repeat or repeat == 1):
        if block_4 % increment == increment - 1:
            block_3 += 1
            block_4 = 0
            r = 1
            if block_3 > 255:
                prefices[prefix] += 1
                prefix = prefices.next_key(prefix)
                block_3 = 0
        else:
            block_4 = block_4 % increment + 1
            r = 1
    else:
        if r < repeat and block_4 >= 0:
            r += 1
        else:
            block_4 += increment
            r = 1
    ip = ".".join([prefix, str(block_3), str(block_4)])
    return ip, prefix, block_3, block_4, r

def generate_IP_increment_2(prefix, block_3, block_4, increment, repeat, r):
    if block_4 + increment > 255:
        if block_4 % increment == increment - 1:
            if r < repeat:
                block_4 = 0
                r += 1
            else:
                block_3 += 1
                block_4 = 0
                r = 1
                if block_3 > 255:
                    prefices[prefix] += 1
                    prefix = prefices.next_key(prefix)
                    block_3 = 0
        else:
            block_4 = block_4 % increment + 1
    else:
        block_4 += increment
    ip = ".".join([prefix, str(block_3), str(block_4)])
    return ip, prefix, block_3, block_4, r

def main():
    parser = argparse.ArgumentParser(description="Generate a CSV file for a DNS query scan")
    parser.add_argument("csv_file", metavar="CSV_FILE", help=HELP[0])
    parser.add_argument("domains_file", metavar="DOMAINS_FILE", help=HELP[1])
    parser.add_argument("pph", metavar='PACKETS_PER_HOUR', type=float, help=HELP[2])
    for args, kwargs in OPTIONS:
        parser.add_argument(*args, **kwargs)
    args = parser.parse_args()

    csv_file = args.csv_file
    pph = args.pph
    domains_file = args.domains_file
    repeat = args.repeat
    pattern = args.pattern
    increment = args.increment

    duration_sec = 3 * 60 * 60
    start_time = 0.0
    end_time = start_time + duration_sec
    time = start_time

    ppm = pph / 60.0
    pps = ppm / 60.0
    if ppm == 0:
        ppm = None
    if pps == 0:
        pps = None

    src = "172.168.0." + str(random.randint(0, 255)) # Generate different source IP address for each attack
    dst_prefix = "129.94"
    block_3 = 0
    block_4 = -1 * increment

    domains = []
    with open(domains_file, "rb") as file:
        for line in file:
            domains.append(line)

    n_domains = len(domains)
    for i in range(n_domains):
        domain = domains[i].rstrip("\n")
        domain = "\\" + domain + "|"
        domains[i] = domain

    rsp_fields = "\\\\,\\\\,\\\\,\\\\,\\\\,\\\\,\\\\,\\\\,\\\\,\\\\,\\\\,\\\\"

    i = 0
    r = 0
    with open(csv_file, "w") as file:
        while time < end_time:
            if increment == 1 and pattern == 0:
                dst, dst_prefix, block_3, block_4, r = generate_IP(dst_prefix, block_3, block_4, repeat, r)
            elif increment == 1 and pattern == 1:
                dst, dst_prefix, block_3, block_4, r = generate_IP_2(dst_prefix, block_3, block_4, repeat, r)
            elif increment != 1 and pattern == 0:
                dst, dst_prefix, block_3, block_4, r = generate_IP_increment(dst_prefix, block_3, block_4, increment, repeat, r)
            else:
                dst, dst_prefix, block_3, block_4, r = generate_IP_increment_2(dst_prefix, block_3, block_4, increment, repeat, r)
            # print(dst)
            qry_id = random.randint(1, 65535)
            domain = domains[i % n_domains]
            print >> file, ",".join([dst, str(int(time)), src, str(qry_id), "true", "StandardQuery", "1", "0", "0", "NoError", \
                                    domain, rsp_fields])
            if ppm == None:
                time += 60.0 * 60.0 / pph
            elif pps == None:
                time += 60.0 / ppm
            else:
                time += 1.0 / pps
            i += 1

if __name__ == "__main__":
    main()
