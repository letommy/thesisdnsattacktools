#!/bin/bash

rates=('600' '1800' '3000' '6000' '18000' '30000' '60000' '180000' '300000' '600000' '1800000' '3000000' '6000000')
domains=('d1.txt' 'd2.txt' 'd3.txt' 'd5.txt' 'd10.txt')

for rate in "${rates[@]}"
do
	for domain in "${domains[@]}"
	do
		length=`wc -l $domain | cut -d" " -f 1`
		echo "Making CSV file for rate=$rate, domain=$length"
		file_name="dns_flood_r$rate"
		file_name+="_d$length.csv"
		echo $file_name
		./dns_flood_csv.py "dns_flood/$file_name" 129.94.0.0 $domain $rate
	done
done