#!/usr/bin/python

import argparse
import random
import collections

HELP = (
    "csv file to write to",
    "target DNS server",
    "txt file of domains to query",
    "number of queries per hour"
)

def main():
    parser = argparse.ArgumentParser(description="Generate a CSV file for a DNS query flood")
    parser.add_argument("csv_file", metavar="CSV_FILE", help=HELP[0])
    parser.add_argument("target", metavar="TARGET", help=HELP[1])
    parser.add_argument("domains_file", metavar="DOMAINS_FILE", help=HELP[2])
    parser.add_argument("pph", metavar='PACKETS_PER_HOUR', type=float, help=HELP[3])
    args = parser.parse_args()

    csv_file = args.csv_file
    dst = args.target
    pph = args.pph
    domains_file = args.domains_file

    duration_sec = 3 * 60 * 60
    start_time = 0.0
    end_time = start_time + duration_sec
    time = start_time

    ppm = pph / 60.0
    pps = ppm / 60.0
    if ppm == 0:
        ppm = None
    if pps == 0:
        pps = None

    src = "172.168.0." + str(random.randint(0, 255)) # Generate different source IP address for each attack

    domains = []
    with open(domains_file, "rb") as file:
        for line in file:
            domains.append(line)

    n_domains = len(domains)
    for i in range(n_domains):
        domain = domains[i].rstrip("\n")
        domain = "\\" + domain + "|"
        domains[i] = domain

    rsp_fields = "\\\\,\\\\,\\\\,\\\\,\\\\,\\\\,\\\\,\\\\,\\\\,\\\\,\\\\,\\\\"

    i = 0
    with open(csv_file, "w") as file:
        while time < end_time:
            qry_id = random.randint(1, 65535)
            domain = domains[i % n_domains]
            print >> file, ",".join([dst, str(int(time)), src, str(qry_id), "true", "StandardQuery", "1", "0", "0", "NoError", \
                                    domain, rsp_fields])
            if ppm == None:
                time += 60.0 * 60.0 / pph
            elif pps == None:
                time += 60.0 / ppm
            else:
                time += 1.0 / pps
            i += 1

if __name__ == "__main__":
    main()
