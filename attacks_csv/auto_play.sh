#!/bin/sh

for csv_file in dns_scan/*.csv
do
    pcap_file=`echo "$csv_file" | sed s/csv/pcap/` 
	echo "Playing file $csv_file"
	start_epoch=`date +%s`
	start_time=`date '+%Y-%m-%d %H:%M:%S'`
	echo "Start: $start_epoch = $start_time"
	sudo timeout 310 tcpdump -i eno1 'port 53' -w "$pcap_file" &
	sudo timeout 300 ./play_csv.py "$csv_file"
	end_epoch=`date +%s`
	end_time=`date '+%Y-%m-%d %H:%M:%S'`
	echo "End: $end_epoch = $end_time\n"
	sleep 300
done