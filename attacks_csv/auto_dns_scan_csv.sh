#!/bin/bash

rates=('60' '180' '300' '600' '1800' '3000' '6000' '18000' '30000' '60000')
domains=('d1.txt' 'd2.txt' 'd3.txt' 'd5.txt' 'd10.txt')
repeats=('1' '2' '3' '5' '10')
patterns=('0' '1')
increments=('1' '2' '3' '5' '10')

for rate in "${rates[@]}"
do
	for domain in "${domains[@]}"
	do
		for repeat in "${repeats[@]}"
		do
			for pattern in "${patterns[@]}"
			do
				for increment in "${increments[@]}"
				do
					length=`wc -l $domain | cut -d" " -f 1`
					echo "Making CSV file for rate=$rate, domain=$length, repeat=$repeat, patterns=$pattern, increments=$increment"
					file_name="dns_scan_r$rate"
					file_name+="_d$length"
					file_name+="_r$repeat"
					file_name+="_p$pattern"
					file_name+="_i$increment.csv"
					echo $file_name
					./dns_flood_csv.py "dns_scan/$file_name" $domain $rate -r $repeat -p $pattern -i $increment
				done
			done
		done
	done
done