# Graph Attacks and Threats
---

## Split Syslog 
---
To split the syslog file into smaller CSV files:
`./split_syslog.sh < log.txt`

The syslog files are split based on the start and end times of an attack, which is determined from the LOG file. Slight modifications to the 'if' conditions in the
'split_syslog.sh' file will need to be made based on the type of attack. The firewall alerts are extracted from the syslog file. The traffic seen by the firewall 
may also be extracted, although this data is not used because the syslog file does not have an entry for every packet.

## Graph
---
These scripts are used to generate graphs for the DNS attacks against the firewall's threat detection system. The graphs are made in a two-step process: make a summary 
CSV file of the traffic and alerts count, and make the graphs from these CSV files.

To generate the graphs:

`./pcap_threats.py PCAP_FILE SYSLOG_THREAT_FILE`

`./graph_pcap_threats_[scan|flood|amp].py TRAFFIC_COUNT_CSV ALERTS_COUNT_CSV`

To generate all the graphs automatically:

`./auto_graph[_scan].sh < log.txt`