#!/bin/bash

while read line
do
	if [[ "$line" == "DNS Flood"* ]]
	then
		read line
		read line
		file_name=`echo "$line" | cut -d/ -f 2`
		echo "$file_name"
		pcap_file=`echo "$file_name" | sed "s/^/pcap\//"`
		threat_file=`echo "$file_name" | sed "s/^/syslog\/threat_/" | sed s/pcap/csv/`
		./pcap_threats.py "$pcap_file" "$threat_file"
		file_name=`echo "$file_name" | sed "s/\.pcap//"`
		traffic_file="graph_csv/traffic_count_$file_name.csv"
		alerts_file="graph_csv/alerts_count_$file_name.csv"
		echo $traffic_file
		echo $alerts_file
		./graph_pcap_threats_flood.py "$traffic_file" "$alerts_file"
	fi
done < "$1"