#!/usr/bin/python

import sys
import re
import time, datetime
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates

directory = "graphs/"
file_name = re.sub(r'^graph_csv/', "", sys.argv[1])
file_name = re.sub(r'\.csv$', "", file_name)

df1 = pd.read_csv(sys.argv[1], header=None, names=['traffic_time', 'traffic_count'])

pattern = '%Y-%m-%d %H:%M:%S'
start_time = int(time.mktime(time.strptime(df1.traffic_time[0], pattern)))

traffic_time = []
for t in df1.traffic_time:
	epoch = int(time.mktime(time.strptime(t, pattern))) - start_time
	traffic_time.append(epoch)
traffic_count = df1.traffic_count

df2 = pd.read_csv(sys.argv[2], header=None, names=['alert_time', 'alert_count', 'severity', 'desc'])

alert_time = []
for t in df2.alert_time:
	epoch = int(time.mktime(time.strptime(t, pattern))) - start_time 
	alert_time.append(epoch)

medium_alert_time = []
medium_alert_count = []
low_alert_time = []
low_alert_count = []
informational_alert_time = []
informational_alert_count = []
for i in range(len(alert_time)):
	if df2.severity[i] == "medium":
		medium_alert_time.append(alert_time[i])
		medium_alert_count.append(df2.alert_count[i])
	elif df2.severity[i] == "low":
		low_alert_time.append(alert_time[i])
		low_alert_count.append(df2.alert_count[i])
	elif df2.severity[i] == "informational":
		informational_alert_time.append(alert_time[i])
		informational_alert_count.append(df2.alert_count[i])

fig, ax1 = plt.subplots()
ax1.plot(traffic_time, traffic_count, label='Traffic from attacker')

ax2 = ax1.twinx()
ax2.scatter(medium_alert_time, medium_alert_count, c='C1', label='SCAN: Host Sweep')
lines1, labels1 = ax1.get_legend_handles_labels()
lines2, labels2 = ax2.get_legend_handles_labels()
ax1.legend(lines1 + lines2, labels1 + labels2, loc='upper center', fontsize='x-large', shadow=True, bbox_to_anchor=(0.5, 1.25))
ax1.set_xlabel('Time (min)',size='x-large')
ax1.set_ylabel('Num. Outgoing DNS Queries/s', size='x-large', color='C0')
ax2.set_ylabel('Alerts', size='x-large', color='C3')
ax1.set_xticks([0, 60, 120, 180, 240, 300])
ax1.set_xticklabels(['0', '1', '2', '3', '4', '5'])
ax1.set_ylim(bottom=0)
ax2.set_ylim(bottom=0, top=1.2)
ax2.set_yticks([0, 1])
ax2.set_yticklabels(['False', 'True'])
ax1.tick_params(labelsize=12)
ax2.tick_params(labelsize=12)
ax1.grid()
fig.savefig(directory + file_name + '.pdf', bbox_inches='tight')
fig.tight_layout()
# plt.shot()