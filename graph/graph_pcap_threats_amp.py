#!/usr/bin/python

import warnings
import sys
import re
import time, datetime
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates

warnings.simplefilter(action='ignore', category=FutureWarning)

directory = "graphs/"
file_name = re.sub(r'^graph_csv/', "", sys.argv[1])
file_name = re.sub(r'\.csv$', "", file_name)

df1 = pd.read_csv(sys.argv[1], header=None, names=['traffic_time', 'traffic_count'])

pattern = '%Y-%m-%d %H:%M:%S'
traffic_time = []
start_time = int(time.mktime(time.strptime(df1.traffic_time[0], pattern)))
end_time = int(time.mktime(time.strptime(df1.traffic_time.iloc[-1], pattern)))
duration = end_time - start_time
for t in df1.traffic_time:
	epoch = int(time.mktime(time.strptime(t, pattern))) - start_time
	traffic_time.append(epoch)
traffic_count = df1.traffic_count

df2 = pd.read_csv(sys.argv[2], header=None, names=['alert_time', 'alert_count', 'severity', 'desc'])

alert_time = []
for t in df2.alert_time:
	epoch = int(time.mktime(time.strptime(t, pattern))) - start_time - 60 * 60
	alert_time.append(epoch)

critical_alert_time = []
critical_alert_count = []
scan_alert_time = []
scan_alert_count = []
dos_alert_time = []
dos_alert_count = []
low_alert_time = []
low_alert_count = []
informational_alert_time = []
informational_alert_count = []
for i in range(len(alert_time)):
	if alert_time[i] < duration:
		if df2.severity[i] == "critical":
			critical_alert_time.append(alert_time[i])
			critical_alert_count.append(df2.alert_count[i])
		elif df2.severity[i] == "medium":
			if df2.desc[i] == "SCAN: UDP Port Scan(8003)":
				scan_alert_time.append(alert_time[i])
				scan_alert_count.append(df2.alert_count[i])
			elif df2.desc[i] == "DNS ANY Queries Brute Force DOS Attack(40033)":
				dos_alert_time.append(alert_time[i])
				dos_alert_count.append(df2.alert_count[i])
		elif df2.severity[i] == "low":
			low_alert_time.append(alert_time[i])
			low_alert_count.append(df2.alert_count[i])
		elif df2.severity[i] == "informational":
			informational_alert_time.append(alert_time[i])
			informational_alert_count.append(df2.alert_count[i])
print("scan alerts @ " + str(scan_alert_time))
print("dos alerts @ " + str(dos_alert_time))
print("informational alerts @ " + str(informational_alert_time))
fig, ax1 = plt.subplots()
ax1.plot(traffic_time, traffic_count, label='Traffic from attacker')
lines1, labels1 = ax1.get_legend_handles_labels()

ax2 = ax1.twinx()
ax3 = ax1.twinx()
ax4 = ax1.twinx()
ax2.scatter(dos_alert_time, dos_alert_count, c='orangered', label='DNS ANY Queries Brute Force DOS Attack')
ax3.scatter(scan_alert_time, scan_alert_count, c='orange', label='SCAN: UDP Port Scan')
ax4.scatter(informational_alert_time, informational_alert_count, c='gold', label='DNS ANY Request')
ax2.set_ylabel('Alerts', size='x-large', color='C3')
ax2.tick_params(axis='y', colors='orangered', labelsize=12)
ax2.set_ylim(bottom=0, top=1.2)
ax2.set_yticks([0, 1])
ax2.set_yticklabels(['False', 'True'])
ax3.tick_params(axis='y', colors='orange', labelsize=12)
ax3.set_ylim(bottom=0, top=1.4)
ax3.set_yticks([0, 1])
ax3.set_yticklabels(['', 'True'])
ax4.tick_params(axis='y', colors='gold', labelsize=12)
ax4.set_ylim(bottom=0, top=1.6)
ax4.set_yticks([0, 1])
ax4.set_yticklabels(['', 'True'])
lines2, labels2 = ax2.get_legend_handles_labels()
lines3, labels3 = ax3.get_legend_handles_labels()
lines4, labels4 = ax4.get_legend_handles_labels()
ax1.legend(lines1 + lines2 + lines3 + lines4, labels1 + labels2 + labels3 + labels4, loc='upper center', fontsize='x-large', shadow=True, bbox_to_anchor=(0.5, 1.4))
ax1.set_xlabel('Time (min)',size='x-large')
ax1.set_ylabel('Num. Outgoing DNS Queries/s', size='x-large', color='C0')
ax1.set_ylim(bottom=0)
ax1.set_xticks([0, 60, 120, 180, 240, 300])
ax1.set_xticklabels(['0', '1', '2', '3', '4', '5'])
ax1.tick_params(labelsize=12)
ax1.grid()
fig.savefig(directory + file_name + '.pdf', bbox_inches='tight')
fig.tight_layout()
# plt.show()