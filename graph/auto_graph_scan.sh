#!/bin/bash

while read line
do
	if [[ "$line" == "Playing"* ]]
	then
		file_name=`echo "$line" | cut -d" " -f 3 | cut -d/ -f 2 | sed "s/\.csv//"`
		echo "$file_name"
		pcap_file="pcap/$file_name.pcap"
		threat_file="syslog/threat_$file_name.csv"
		./pcap_threats.py "$pcap_file" "$threat_file"
		traffic_file="graph_csv/traffic_count_$file_name.csv"
		alerts_file="graph_csv/alerts_count_$file_name.csv"
		./graph_pcap_threats_scan.py "$traffic_file" "$alerts_file"
	fi
done < "$1"