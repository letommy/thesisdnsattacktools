#!/bin/bash

while read line
do
	if [[ "$line" == "DNS"* ]]
	then
		read line
		start=`echo "$line" | sed 's/^.*= //' | sed "s/-/\//g" | sed "s/:[0-9][0-9]$//"`
		read line
		file_name=`echo "$line" | cut -d/ -f 2`
		echo "$file_name"
		read line
		end=`echo "$line" | sed 's/^.*= //' | sed "s/-/\//g"`
		end=`date --date="$end" +"%s"`
		end=$(( $end + 60 ))
		end=`date -d @"$end" +"%Y/%m/%d %H:%M"`
		traffic_file=`echo "$file_name" | sed "s/^/syslog\/traffic_/" | sed "s/pcap/csv/"`
		threat_file=`echo "$file_name" | sed "s/^/syslog\/threat_/" | sed "s/pcap/csv/"`
		echo "$threat_file"
		# egrep "$start" syslog/syslog_splitaa -A 1000000 | egrep "$end" -B 1000000 | egrep "TRAFFIC" > "$traffic_file"
		# egrep "$start" syslog/syslog_splitab -A 1000000 | egrep "$end" -B 1000000 | egrep "TRAFFIC" >> "$traffic_file"
		egrep "$start" syslog/threat.csv -A 1000000 | egrep "$end" -B 1000000 | egrep "THREAT" | egrep -v ",spyware," > "$threat_file"
		egrep "$start" syslog/syslog -A 1000000 | egrep "$end" -B 1000000 | egrep "THREAT" | egrep -v ",spyware," >> "$threat_file"
		# egrep "$start" syslog/syslog_splitab -A 1000000 | egrep "$end" -B 1000000 | egrep "THREAT" | egrep -v ",spyware," >> "$threat_file"
	fi
done < "$1"