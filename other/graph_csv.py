#!/usr/bin/python

import sys
import re
import datetime
import collections
import pandas as pd
import matplotlib.pyplot as plt

directory = re.match(r'^.*/', sys.argv[1]).group(0)
file_name = sys.argv[1].split('/')[-1]
file_name = re.sub(r'\.csv', '', file_name)

csv_count = collections.OrderedDict()
csv_volume = collections.OrderedDict()

df = pd.read_csv(sys.argv[1], header=None, usecols=[0, 1, 2, 3], names=['time', 'src', 'dst', 'length'])

start_time_csv = df.time[0]

for i in range(len(df.time)):
	if df.dst[i] == "10.0.0.3":
		time = int(df.time[i]) - start_time_csv
		try:
			csv_count[time] += 1
			csv_volume[time] += df.length[i]
		except KeyError:
			csv_count[time] = 1
			csv_volume[time] = df.length[i]

for key, value in csv_volume.iteritems():
	csv_volume[key] = value * 8 / 1000000.0

fig1, ax1 = plt.subplots()
ax1.plot(csv_count.keys(), csv_count.values(), linewidth=2)
ax1.set_xlabel('Time (s)',size='x-large')
ax1.set_ylabel('Num. of Incoming Pkts/s', size='x-large')
ax1.tick_params(labelsize=12)
ax1.grid()
fig1.savefig(directory + file_name + "_count.pdf", bbox_inches='tight')
fig1.tight_layout()
# plt.show()

fig2, ax2 = plt.subplots()
ax2.plot(csv_volume.keys(), csv_volume.values(), linewidth=2)
ax2.set_xlabel('Time (s)',size='x-large')
ax2.set_ylabel('Volume (Mbps)', size='x-large')
ax2.tick_params(labelsize=12)
ax2.grid()
fig2.savefig(directory + file_name + "_volume.pdf", bbox_inches='tight')
fig2.tight_layout()
# plt.show()