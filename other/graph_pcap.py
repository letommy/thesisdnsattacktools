#!/usr/bin/python

import sys
import re
import datetime
import collections
import dpkt
import pandas as pd
import matplotlib.pyplot as plt

directory = re.match(r'^.*/', sys.argv[1]).group(0)
file_name = sys.argv[1].split('/')[-1]
file_name = re.sub(r'\.pcap', '', file_name)

start_time_pcap = None

pcap_count = collections.OrderedDict()
pcap_volume = collections.OrderedDict()

pcap_file = open(sys.argv[1])
pcap = dpkt.pcap.Reader(pcap_file)

for ts, buf in pcap:
    if start_time_pcap == None:
        start_time_pcap = int(ts)
        # for i in range(15):
        # 	pcap_count[i] = 0
        # 	pcap_volume[i] = 0
    time = int(ts) - start_time_pcap
    try:
        pcap_count[time] += 1
        pcap_volume[time] += len(buf)
    except KeyError:
        pcap_count[time] = 1
        pcap_volume[time] = len(buf)
    last = time

# for i in range(15):
# 	pcap_count[last + i] = 1
# 	pcap_volume[last + i] = 0

for key, value in pcap_volume.iteritems():
	pcap_volume[key] = value * 8 / 1000000.0

fig1, ax1 = plt.subplots()
ax1.plot(pcap_count.keys(), pcap_count.values(), linewidth=2)
ax1.set_xlabel('Time (s)',size='x-large')
ax1.set_ylabel('Num. of Incoming Pkts/s', size='x-large')
ax1.tick_params(labelsize=12)
ax1.grid()
fig1.savefig(directory + file_name + "_count.pdf", bbox_inches='tight')
fig1.tight_layout()
# plt.show()

fig2, ax2 = plt.subplots()
ax2.plot(pcap_volume.keys(), pcap_volume.values(), linewidth=2)
ax2.set_xlabel('Time (s)',size='x-large')
ax2.set_ylabel('Volume (Mbps)', size='x-large')
ax2.tick_params(labelsize=12)
ax2.grid()
fig2.savefig(directory + file_name + "_volume.pdf", bbox_inches='tight')
fig2.tight_layout()
# plt.show()