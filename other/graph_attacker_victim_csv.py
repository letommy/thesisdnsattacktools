#!/usr/bin/python

import sys
import re
import datetime
import collections
import dpkt
import pandas as pd
import matplotlib.pyplot as plt

directory = 'graphs/'
file_name = sys.argv[1].split('/')[-1]
file_name = re.sub(r'\.pcap', '', file_name)

df1 = pd.read_csv(sys.argv[1], header=None, usecols=[0, 1, 2, 4], names=['time', 'src', 'dst', 'length'])
df2 = pd.read_csv(sys.argv[2], header=None, usecols=[0, 1, 2, 3], names=['time', 'src', 'dst', 'length'])

attacker_start_time = int(df1.time[0])
victim_start_time = int(df2.time[0])

attacker_count = collections.OrderedDict()
attacker_volume = collections.OrderedDict()

time = 0
while time < 15:
    attacker_count[time] = 0
    attacker_volume[time] = 0
    time += 1

for i in range(len(df1.time)):
    if df1.src[i] == "10.0.0.3":
        time = int(df1.time[i]) - attacker_start_time
        try:
            attacker_count[time] += 1
            attacker_volume[time] += df1.length[i]
        except KeyError:
            attacker_count[time] = 1
            attacker_volume[time] = df1.length[i]
        attacker_end_time = time

time = attacker_end_time
while time < attacker_end_time + 15:
    attacker_count[time] = 0
    attacker_volume[time] = 0
    time += 1

for key, value in attacker_volume.iteritems():
    attacker_volume[key] = float(value) * 8 / 1000000.0

victim_count = collections.OrderedDict()
victim_volume = collections.OrderedDict()

time = 0
while time < df2.time[0] - victim_start_time:
    victim_count[time] = 0
    victim_volume[time] = 0
    time += 1

for i in range(len(df2.time)):
    if df2.dst[i] == "10.0.0.3":
        time = int(df2.time[i]) - victim_start_time
        try:
            victim_count[time] += 1
            victim_volume[time] += df2.length[i]
        except KeyError:
            victim_count[time] = 1
            victim_volume[time] = df2.length[i]
        victim_end_time = time

time = victim_end_time
while time < attacker_end_time + 15:
    victim_count[time] = 0
    victim_volume[time] = 0
    time += 1

for key, value in victim_volume.iteritems():
    victim_volume[key] = float(value) * 8 / 1000000.0

fig1, ax1 = plt.subplots()
ax1.plot(attacker_count.keys(), attacker_count.values(), label='attacker', linewidth=3)
ax1.plot(victim_count.keys(), victim_count.values(), label='victim', linewidth=2)
ax1.legend(loc='upper right', fontsize='x-large', shadow=True)
ax1.set_xlabel('Time (s)',size='x-large')
ax1.set_ylabel('Num. of Pkts/s', size='x-large')
ax1.tick_params(labelsize=12)
ax1.grid()
fig1.savefig(directory + file_name + "_count.pdf", bbox_inches='tight')
fig1.tight_layout()
# plt.show()

fig2, ax2 = plt.subplots()
ax2.plot(attacker_volume.keys(), attacker_volume.values(), label='attacker', linewidth=3)
ax2.plot(victim_volume.keys(), victim_volume.values(), label='victim', linewidth=2)
ax2.legend(loc='upper right', fontsize='x-large', shadow=True)
ax2.set_xlabel('Time (s)',size='x-large')
ax2.set_ylabel('Volume (Mbps)', size='x-large')
ax2.tick_params(labelsize=12)
ax2.grid()
fig2.savefig(directory + file_name + "_volume.pdf", bbox_inches='tight')
fig2.tight_layout()
# plt.show()