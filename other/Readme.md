# Other Graph Tools
---
These scripts are used to generate graphs for the outgoing/incoming rates. The data may be collected in PCAP or CSV format, therefore there are two versions of the 
graphing scripts.

To generate the graph from a PCAP file:

`./graph_pcap.py PCAP_FILE`

To generate the graph from a CSV file:

`./graph_csv.py CSV_FILE`

To generate the graph for the outgoing rate at the attacker and the incoming rate at the victim from PCAP files:

`./graph_attacker_victim_pcap.py ATTACKER_PCAP_FILE VICTIM_PCAP_FILE`

To generate the graph for the outgoing rate at the attacker and the incoming rate at the victim from CSV files:

`./graph_attacker_victim_csv.py ATTACKER_CSV_FILE VICTIM_CSV_FILE`