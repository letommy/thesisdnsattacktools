#!/usr/bin/python

import sys
import re
import datetime
import collections
import dpkt
import pandas as pd
import matplotlib.pyplot as plt

directory = 'graphs/'
file_name = sys.argv[1].split('/')[-1]
file_name = re.sub(r'\.pcap', '', file_name)

attacker_start_time = None

attacker_count = collections.OrderedDict()
attacker_volume = collections.OrderedDict()

attacker_file = open(sys.argv[1])
attacker_pcap = dpkt.pcap.Reader(attacker_file)

for ts, buf in attacker_pcap:
    if attacker_start_time == None:
        attacker_start_time = int(ts)
        for i in range(15):
        	attacker_count[i] = 1
        	attacker_volume[i] = 0
    time = int(ts) - attacker_start_time + 15
    try:
        attacker_count[time] += 1
        attacker_volume[time] += len(buf)
    except KeyError:
        attacker_count[time] = 1
        attacker_volume[time] = len(buf)
    last = time

for i in range(15):
	attacker_count[last + i] = 1
	attacker_volume[last + i] = 0

for key, value in attacker_volume.iteritems():
	attacker_volume[key] = value * 8 / 1000000.0
victim_start_time = None

victim_count = collections.OrderedDict()
victim_volume = collections.OrderedDict()

victim_file = open(sys.argv[2])
victim_pcap = dpkt.pcap.Reader(victim_file)

for ts, buf in victim_pcap:
    if victim_start_time == None:
        victim_start_time = int(ts)
        for i in range(15):
            victim_count[i] = 1
            victim_volume[i] = 0
    time = int(ts) - victim_start_time + 15
    try:
        victim_count[time] += 1
        victim_volume[time] += len(buf)
    except KeyError:
        victim_count[time] = 1
        victim_volume[time] = len(buf)
    last = time

for i in range(15):
    victim_count[last + i] = 1
    victim_volume[last + i] = 0

for key, value in victim_volume.iteritems():
    victim_volume[key] = value * 8 / 1000000.0
print(attacker_count)
print(victim_count)
fig1, ax1 = plt.subplots()
ax1.plot(attacker_count.keys(), attacker_count.values(), label='attacker', linewidth=3)
ax1.plot(victim_count.keys(), victim_count.values(), label='victim', linewidth=2)
ax1.legend(loc='upper right', fontsize='x-large', shadow=True)
ax1.set_xlabel('Time (s)',size='x-large')
ax1.set_ylabel('Num. of Pkts/s', size='x-large')
ax1.tick_params(labelsize=12)
ax1.grid()
fig1.savefig(directory + file_name + "_count.pdf", bbox_inches='tight')
fig1.tight_layout()
# plt.show()

fig2, ax2 = plt.subplots()
ax2.plot(attacker_volume.keys(), attacker_volume.values(), label='attacker', linewidth=3)
ax2.plot(victim_volume.keys(), victim_volume.values(), label='victim', linewidth=2)
ax2.legend(loc='upper right', fontsize='x-large', shadow=True)
ax2.set_xlabel('Time (s)',size='x-large')
ax2.set_ylabel('Volume (Mbps)', size='x-large')
ax2.tick_params(labelsize=12)
ax2.grid()
fig2.savefig(directory + file_name + "_volume.pdf", bbox_inches='tight')
fig2.tight_layout()
# plt.show()