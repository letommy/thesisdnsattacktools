#!/bin/bash

rates=('3000' '5000' '10000' '30000')
domains=('d1.txt')
duration=300
ramp='1,30000'
sawtooths=('1,3000,3' '1,5000,3' '1,10000,3' '1,30000,3')

for rate in "${rates[@]}"
do
	for domain in "${domains[@]}"
	do
		echo "DNS Flood: rate=$rate"
		start_epoch=`date +%s`
		start_time=`date '+%Y-%m-%d %H:%M:%S'`
		echo "Start: $start_epoch = $start_time"
		length=`wc -l $domain | cut -d" " -f 1`
		file_name="dns_flood/dns_flood_r$rate"
		file_name+="_d$length.pcap"
		echo "$file_name"
		sudo timeout 310 tcpdump -i eno1 'port 53' -w $file_name &
		sudo ./dns_flood.py 10.0.0.3 $domain 'A' $duration -r $rate
		end_epoch=`date +%s`
		end_time=`date '+%Y-%m-%d %H:%M:%S'`
		echo -e "End: $end_epoch = $end_time\n"
		sleep 300
	done
done

 
for domain in "${domains[@]}"
do
	echo "DNS Flood: ramp=$ramp"
	start_epoch=`date +%s`
	start_time=`date '+%Y-%m-%d %H:%M:%S'`
	echo "Start: $start_epoch = $start_time"
	length=`wc -l $domain | cut -d" " -f 1`
	file_name="dns_flood/dns_flood_ramp$ramp"
	file_name+="_d$length.pcap"
	echo "$file_name"
	sudo timeout 310 tcpdump -i eno1 'port 53' -w $file_name &
	sudo ./dns_flood.py 10.0.0.3 $domain 'A' $duration --ramp $ramp
	end_epoch=`date +%s`
	end_time=`date '+%Y-%m-%d %H:%M:%S'`
	echo -e "End: $end_epoch = $end_time\n"
	sleep 300
done

for sawtooth in "${sawtooths[@]}"
do 
	for domain in "${domains[@]}"
	do
		echo "DNS Flood: sawtooth=$sawtooth"
		start_epoch=`date +%s`
		start_time=`date '+%Y-%m-%d %H:%M:%S'`
		echo "Start: $start_epoch = $start_time"
		length=`wc -l $domain | cut -d" " -f 1`
		file_name="dns_flood/dns_flood_sawtooth$sawtooth"
		file_name+="_d$length.pcap"
		echo "$file_name"
		sudo timeout 310 tcpdump -i eno1 'port 53' -w $file_name &
		sudo ./dns_flood.py 10.0.0.3 $domain 'A' $duration --sawtooth $sawtooth
		end_epoch=`date +%s`
		end_time=`date '+%Y-%m-%d %H:%M:%S'`
		echo -e "End: $end_epoch = $end_time\n"
		sleep 300
	done
done

rates=('30' '50' '100' '30000')
domains=('cpsc.txt')
sawtooths=('1,30,3' '1,50,3' '1,100,3' '1,30000,3')

for rate in "${rates[@]}"
do
	for domain in "${domains[@]}"
	do
		echo "DNS Amplification: rate=$rate"
		start_epoch=`date +%s`
		start_time=`date '+%Y-%m-%d %H:%M:%S'`
		echo "Start: $start_epoch = $start_time"
		length=`wc -l $domain | cut -d" " -f 1`
		file_name="dns_amp/dns_amp_r$rate"
		file_name+="_d$length"
		file_name+="_A.pcap"
		echo "$file_name"
		sudo timeout 310 tcpdump -i eno1 'port 53' -w $file_name &
		sudo ./dns_amp.py 10.0.0.3 $domain 'A' $duration -r $rate
		end_epoch=`date +%s`
		end_time=`date '+%Y-%m-%d %H:%M:%S'`
		echo -e "End: $end_epoch = $end_time\n"
		sleep 300
	done
done

for domain in "${domains[@]}"
do
	echo "DNS Amplification: ramp=$ramp"
	start_epoch=`date +%s`
	start_time=`date '+%Y-%m-%d %H:%M:%S'`
	echo "Start: $start_epoch = $start_time"
	length=`wc -l $domain | cut -d" " -f 1`
	file_name="dns_amp/dns_amp_ramp$ramp"
	file_name+="_d$length"
	file_name+="_A.pcap"
	echo "$file_name"
	sudo timeout 310 tcpdump -i eno1 'port 53' -w $file_name &
	sudo ./dns_amp.py 10.0.0.3 $domain 'A' $duration --ramp $ramp
	end_epoch=`date +%s`
	end_time=`date '+%Y-%m-%d %H:%M:%S'`
	echo -e "End: $end_epoch = $end_time\n"
	sleep 300
done

for sawtooth in "${sawtooths[@]}"
do 
	for domain in "${domains[@]}"
	do
		echo "DNS Amplification: sawtooth=$sawtooth"
		start_epoch=`date +%s`
		start_time=`date '+%Y-%m-%d %H:%M:%S'`
		echo "Start: $start_epoch = $start_time"
		length=`wc -l $domain | cut -d" " -f 1`
		file_name="dns_amp/dns_amp_sawtooth$sawtooth"
		file_name+="_d$length"
		file_name+="_A.pcap"
		echo "$file_name"
		sudo timeout 310 tcpdump -i eno1 'port 53' -w $file_name &
		sudo ./dns_amp.py 10.0.0.3 $domain 'A' $duration --sawtooth $sawtooth
		end_epoch=`date +%s`
		end_time=`date '+%Y-%m-%d %H:%M:%S'`
		echo -e "End: $end_epoch = $end_time\n"
		sleep 300
	done
done

rates=('5' '10' '30' '50' '30000')
sawtooths=('1,5,3' '1,10,3' '1,30,3' '1,50,3' '1,30000,3')

for rate in "${rates[@]}"
do
	for domain in "${domains[@]}"
	do
		echo "DNS Amplification: rate=$rate"
		start_epoch=`date +%s`
		start_time=`date '+%Y-%m-%d %H:%M:%S'`
		echo "Start: $start_epoch = $start_time"
		length=`wc -l $domain | cut -d" " -f 1`
		file_name="dns_amp/dns_amp_r$rate"
		file_name+="_d$length"
		file_name+="_ANY.pcap"
		echo "$file_name"
		sudo timeout 310 tcpdump -i eno1 'port 53' -w $file_name &
		sudo ./dns_amp.py 10.0.0.3 $domain 'ANY' $duration -r $rate
		end_epoch=`date +%s`
		end_time=`date '+%Y-%m-%d %H:%M:%S'`
		echo -e "End: $end_epoch = $end_time\n"
		sleep 90
	done
done

for domain in "${domains[@]}"
do
	echo "DNS Amplification: ramp=$ramp"
	start_epoch=`date +%s`
	start_time=`date '+%Y-%m-%d %H:%M:%S'`
	echo "Start: $start_epoch = $start_time"
	length=`wc -l $domain | cut -d" " -f 1`
	file_name="dns_amp/dns_amp_ramp$ramp"
	file_name+="_d$length"
	file_name+="_ANY.pcap"
	echo "$file_name"
	sudo timeout 310 tcpdump -i eno1 'port 53' -w $file_name &
	sudo ./dns_amp.py 10.0.0.3 $domain 'ANY' $duration --ramp $ramp
	end_epoch=`date +%s`
	end_time=`date '+%Y-%m-%d %H:%M:%S'`
	echo -e "End: $end_epoch = $end_time\n"
	sleep 90
done

for sawtooth in "${sawtooths[@]}"
do 
	for domain in "${domains[@]}"
	do
		echo "DNS Amplification: sawtooth=$sawtooth"
		start_epoch=`date +%s`
		start_time=`date '+%Y-%m-%d %H:%M:%S'`
		echo "Start: $start_epoch = $start_time"
		length=`wc -l $domain | cut -d" " -f 1`
		file_name="dns_amp/dns_amp_sawtooth$sawtooth"
		file_name+="_d$length"
		file_name+="_ANY.pcap"
		echo "$file_name"
		sudo timeout 310 tcpdump -i eno1 'port 53' -w $file_name &
		sudo ./dns_amp.py 10.0.0.3 $domain 'ANY' $duration --sawtooth $sawtooth
		end_epoch=`date +%s`
		end_time=`date '+%Y-%m-%d %H:%M:%S'`
		echo -e "End: $end_epoch = $end_time\n"
		sleep 90
	done
done