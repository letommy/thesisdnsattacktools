# Generate Attacks From CSV
---
These scripts are used to generate a CSV file for DNS attacks and replay them from the CSV file.

## Generate DNS Query Flood
---
To generate a DNS query flood:

`sudo ./dns_flood.py [-h] [-r N] [-a L,U] [-b L,U,N] TARGET DOMAINS_FILE RECORD DURATION`

|Positional Argument|Info|
|---|---|
|TARGET          |target DNS server|
|DOMAINS_FILE    |files containing the list of domains to query|
|RECORD			 |record type|
|DURATION        |duration of the attack in seconds|

|Optional Argument|Info|
|---|---|
|-h, --help				    |show a help message and exit|
|-r N, --rate N		        |rate in packets per second|
|-a L,U, --ramp L,U	    	|increase rate in the shape of a ramp|
|-b L,U,N, --sawtooth L,U,N	|increase rate in the shape of a sawtooth|

e.g. `sudo ./dns_flood.py 10.0.0.43 d5.txt 300 A --ramp 1,100`

where:

- '10.0.0.43' is the target DNS server
- 'd5.txt' is the name of the TXT file which contains the list of domains to query
- 'A' is the query type
- '300' is the duration of the DNS query flood
- '--ramp 1,100' indicates that the rate increases in the shape of a ramp, where '1' is the min rate and '100' is the max rate

## Generate DNS Amplification Attack
---
To generate a DNS amplification attack:

`sudo ./dns_amp.py [-h] [-r N] [-a L,U] [-b L,U,N] TARGET DNS_SERVER DOMAINS_FILE RECORD DURATION`

|Positional Argument|Info|
|---|---|
|TARGET          |target/victim|
|DNS_SERVER		 |reflecting DNS server|
|DOMAINS_FILE    |files containing the list of domains to query|
|RECORD			 |record type|
|DURATION        |duration of the attack in seconds|

|Optional Argument|Info|
|---|---|
|-h, --help				    |show a help message and exit|
|-r N, --rate N		        |rate in packets per second|
|-a L,U, --ramp L,U	    	|increase rate in the shape of a ramp|
|-b L,U,N, --sawtooth L,U,N	|increase rate in the shape of a sawtooth|

e.g. `sudo ./dns_amp.py 10.0.0.3 172.168.0.63 d5.txt ANY 300 --sawtooth 1,100,3`

where:

- '10.0.0.3' is the target (web server)
- '172.168.0.63' is the reflecting DNS server
- 'd5.txt' is the name of the TXT file which contains the list of domains to query
- 'ANY' is the query type
- '300' is the duration of the DNS amplification attack
- '--sawtooth 1,100,3' indicates that the rate increases in the shape of a sawtooth, where '1' is the min rate, '100' is the max rate and '3' is the number of sawtooths

## Automate Attacks
---
To automate the attacks:

`sudo ./auto_attack.sh >> log.txt`

The log.txt file contains a description of each attack as well as their start and end times.

Modify the parameters in the arrays in the auto_attack.sh file accordingly.

## Other Tools
---
The following scripts were developed to generate DNS query scans and TCP SYN floods. They haven't been tested extensively. As such, modifications may be required.

### DNS Query Scan

To generate a DNS query scan:

`sudo ./prelim_dns_scan.py [-r RATE] TARGET TARGET_MASK DOMAIN`

e.g. `sudo ./prelim_dns_scan.py 10.0.0.0 24 www.google.com -r 100`

where:

- '10.0.0.0' is the target IP address
- '24' is the target IP address mask/subnet (i.e. the 10.0.0.0/24 address block will be scanned)
- 'www.google.com' is the domain to query
- '-r 100' is the rate in packets per second at which the scanner is sending queries

### TCP SYN Flood

To generate a TCP SYN flood:

`sudo ./prelim_syn_flood.py [-r RATE] TARGET`

e.g. `sudo ./prelim_syn_flood.py 10.0.0.3 -r 100`

where:

- '10.0.0.3' is the target (web server)
- '-r 100' is the rate in packets per second at which the flooder is sending queries