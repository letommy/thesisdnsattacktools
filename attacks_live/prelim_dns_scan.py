#!/usr/bin/python

import sys
import re
import time
import argparse
import random
import socket
from scapy.all import *
from pinject import IP, TCP

HELP = (
    "target address",
    "target address mask",
    "domain to query",
    "rate: number of queries per second (default=10)",
    "number of threads to run",
)

OPTIONS = (
    (('-r', '--rate'), dict(dest='rate', type=float,
                            metavar='N', default=10.0, help=HELP[3])),
    (('-t', '--threads'), dict(dest='threads',
                               metavar='N', default=1, help=HELP[4]))
)

def scan(address, mask, domain, rate):
    if not isIPv4(address):
        print("Address is not valid, exiting...")
        return
    s = conf.L2socket()
    dns_servers = []
    if mask == 24:
        for i in range(256):
            try:
                block4 = address.split(".")[3]
                address = re.sub(r"%s$" % block4, str(i), address)
                packs = sr1(IP(dst=address) / UDP(sport=RandShort(), dport=53) / \
                    DNS(id=RandShort(), rd=1, qd=DNSQR(qname=domain, qtype="A", qclass="IN")), timeout=1/rate, verbose=0)
                if packs is not None:
                    print("DNS server found: " + address)
                    dns_servers.append(address)
                # else:
                #     print("No valid DNS server: " + address)
            except KeyboardInterrupt:
                print("\nInterrupted...")
                print(time.strftime("%Y-%m-%d %H:%M:%S"))
                sys.exit()
    return dns_servers

def isIPv4(addr):
    try:
        socket.inet_aton(addr)
        return True
    except socket.error:
        return False

def main():
    print(time.strftime("%Y-%m-%d %H:%M:%S"))
    parser = argparse.ArgumentParser(description="Generate a DNS query scan")
    parser.add_argument("address", metavar='ADDRESS', help=HELP[0])
    parser.add_argument("mask", metavar="MASK", type=int, help=HELP[1])
    parser.add_argument("domain", metavar='DOMAIN_NAME', default="www.google.com", help=HELP[2])
    for args, kwargs in OPTIONS:
        parser.add_argument(*args, **kwargs)
    args = parser.parse_args()
    
    dns_servers = scan(args.address, args.mask, args.domain, args.rate)
    print("DNS Servers found: " + str(dns_servers))
    print(time.strftime("%Y-%m-%d %H:%M:%S"))

if __name__ == "__main__":
    main()
