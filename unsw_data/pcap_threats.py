#!/usr/bin/python

import sys
import re
import time, datetime
import socket
import dpkt
from collections import OrderedDict
import pandas as pd

def inet_to_str(inet):
    """Convert inet object to a string
        Args:
            inet (inet struct): inet network address
        Returns:
            str: Printable/readable IP address
    """
    # First try ipv4 and then ipv6
    try:
        return socket.inet_ntop(socket.AF_INET, inet)
    except ValueError:
        return socket.inet_ntop(socket.AF_INET6, inet)

attacker = re.search(r'\d+\.\d+\.\d+\.\d+', sys.argv[1]).group(0)

start_time_csv = 1525096800
start_time_pcap = 1537838350
end_time_pcap = start_time_pcap + 24 * 60 * 60

traffic_count = OrderedDict()
alert_count = OrderedDict()
times = []

pcap_file = open(sys.argv[1])
pcap = dpkt.pcap.Reader(pcap_file)
interval = 0
block = 0

print("reading pcap file")
for ts, buf in pcap:
    eth = dpkt.ethernet.Ethernet(buf)
    ip = eth.data 
    if inet_to_str(ip.src) == attacker:
        if interval == 0:
            interval = ts
            prev_ts = start_time_pcap
        if ts > prev_ts + 10:
            while ts > prev_ts + 1:
                t = prev_ts - start_time_pcap + start_time_csv
                t = datetime.datetime.utcfromtimestamp(float(int(t) + 36000))
                traffic_count[t] = 0
                prev_ts += 10
        if ts > interval + 1:
            t = interval - start_time_pcap + start_time_csv 
            t = datetime.datetime.utcfromtimestamp(float(int(t) + 36000))
            try:
                traffic_count[t] += block
            except KeyError:
                traffic_count[t] = block
            interval = ts
            block = 0
        else:
            block += 1
        prev_ts = interval

while prev_ts < end_time_pcap:
    t = prev_ts - start_time_pcap + start_time_csv
    t = datetime.datetime.utcfromtimestamp(float(int(t) + 36000))
    traffic_count[t] = 0
    prev_ts += 10

df = pd.read_csv(sys.argv[2], header=None, usecols=[1, 4, 7, 8, 30, 32, 34], \
    names=['date_time', 'type', 'src', 'dst', 'action', 'threat_desc', 'severity'])

print("reading csv file")
for i in range(len(df.date_time)):
    if "Suspicious" not in df.threat_desc[i]:
        if df.src[i] == attacker:
            date_time = df.date_time[i]
            pattern = '%Y/%m/%d %H:%M:%S'
            epoch = int(time.mktime(time.strptime(date_time, pattern)))
            epoch = epoch - start_time_pcap + start_time_csv 
            t = datetime.datetime.utcfromtimestamp(float(epoch + 36000))
            if df.severity[i] == "critical":
                try:
                    alert_count[t]['count'] += 1
                except KeyError:
                    alert_count[t] = {}
                    alert_count[t]['count'] += 1
                    alert_count[t]['severity'] = "critical"
            elif df.severity[i] == "medium":
                try:
                    alert_count[t]['count'] += 1
                except KeyError:
                    alert_count[t] = {}
                    alert_count[t]['count'] = 1
                    alert_count[t]['severity'] = "medium"
            elif df.severity[i] == "low":
                try:
                    alert_count[t]['count'] += 1
                except KeyError:
                    alert_count[t] = {}
                    alert_count[t]['count'] += 1
                    alert_count[t]['severity'] = "low"
            elif df.severity[i] == "informational":
                try:
                    alert_count[t]['count'] += 1
                except KeyError:
                    alert_count[t] = {}
                    alert_count[t]['count'] += 1
                    alert_count[t]['severity'] = "informational"

print("writing to csv file")
directory = re.match(r'UNSW-\d+-\d+', sys.argv[1]).group(0)
df1 = pd.DataFrame.from_dict(traffic_count, orient="index")
df1.to_csv(directory + "/traffic_count_" + attacker + ".csv", header=False)
df2 = pd.DataFrame.from_dict(alert_count, orient="index")
df2.to_csv(directory + "/alerts_count_" + attacker + ".csv", header=False)
