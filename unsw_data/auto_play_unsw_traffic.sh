#!/bin/sh

epoch=`date +%s`
time=`date '+%Y-%m-%d %H:%M:%S'`
echo "Start: $epoch = $time"
sudo timeout 90000 tcpdump -i eno1 port 53 -w 0105.pcap &
sudo ./play_csv.py query_filtered_UNSW-01-05.csv 
epoch=`date +%s`
time=`date '+%Y-%m-%d %H:%M:%S'`
echo "End: $epoch = $time"
sleep 3600

epoch=`date +%s`
time=`date '+%Y-%m-%d %H:%M:%S'`
echo "Start: $epoch = $time"
sudo timeout 90000 tcpdump -i eno1 port 53 -w 0205.pcap &
sudo ./play_csv.py query_filtered_UNSW-02-05.csv 
epoch=`date +%s`
time=`date '+%Y-%m-%d %H:%M:%S'`
echo "End: $epoch = $time"
sleep 3600

epoch=`date +%s`
time=`date '+%Y-%m-%d %H:%M:%S'`
echo "Start: $epoch = $time"
sudo timeout 90000 tcpdump -i eno1 port 53 -w 0305.pcap &
sudo ./play_csv.py query_filtered_UNSW-03-05.csv 
epoch=`date +%s`
time=`date '+%Y-%m-%d %H:%M:%S'`
echo "End: $epoch = $time"
sleep 3600

epoch=`date +%s`
time=`date '+%Y-%m-%d %H:%M:%S'`
echo "Start: $epoch = $time"
sudo timeout 90000 tcpdump -i eno1 port 53 -w 0405.pcap &
sudo ./play_csv.py query_filtered_UNSW-04-05.csv 
epoch=`date +%s`
time=`date '+%Y-%m-%d %H:%M:%S'`
echo "End: $epoch = $time"
sleep 3600

epoch=`date +%s`
time=`date '+%Y-%m-%d %H:%M:%S'`
echo "Start: $epoch = $time"
sudo timeout 90000 tcpdump -i eno1 port 53 -w 0505.pcap &
sudo ./play_csv.py query_filtered_UNSW-05-05.csv
epoch=`date +%s`
time=`date '+%Y-%m-%d %H:%M:%S'`
echo "End: $epoch = $time" 
sleep 3600

epoch=`date +%s`
time=`date '+%Y-%m-%d %H:%M:%S'`
echo "Start: $epoch = $time"
sudo timeout 90000 tcpdump -i eno1 port 53 -w 0605.pcap &
sudo ./play_csv.py query_filtered_UNSW-06-05.csv 
epoch=`date +%s`
time=`date '+%Y-%m-%d %H:%M:%S'`
echo "End: $epoch = $time"
sleep 3600

epoch=`date +%s`
time=`date '+%Y-%m-%d %H:%M:%S'`
echo "Start: $epoch = $time"
sudo timeout 90000 tcpdump -i eno1 port 53 -w 0705.pcap &
sudo ./play_csv.py query_filtered_UNSW-07-05.csv 
epoch=`date +%s`
time=`date '+%Y-%m-%d %H:%M:%S'`
echo "End: $epoch = $time"