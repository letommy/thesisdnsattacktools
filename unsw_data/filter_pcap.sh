#!/bin/bash

ips=('185.94.111.1' '60.247.99.245' '71.6.146.186' '213.152.168.178' '109.202.109.108')

for ip in ${ips[@]}
do
	echo "filtering pcap for $ip"
	dir=`echo "$1" | sed 's/\/.*//'`
	output_pcap="$dir"
	output_pcap+="/src_$ip.pcap"
	echo "$output_pcap"
	tcpdump -r "$1" src "$ip" -w "$output_pcap"
done