#!/usr/bin/python

import sys
import re
import datetime
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates

attacker = re.search(r'\d+\.\d+\.\d+\.\d+', sys.argv[1]).group(0)
directory = re.match(r'UNSW-\d+-\d+', sys.argv[1]).group(0)

df1 = pd.read_csv(sys.argv[1], header=None, names=['time', 'traffic_count'])

traffic_time = []
pattern = '%Y-%m-%d %H:%M:%S'
for t in df1.time:
	t = datetime.datetime.strptime(t, pattern)
	traffic_time.append(t)
traffic_count = df1.traffic_count

df2 = pd.read_csv(sys.argv[2], header=None, names=['time', 'alert_count', 'severity'])

medium_alert_time = []
medium_alert_count = []
for i in range(len(df2.time)):
	t = datetime.datetime.strptime(df2.time[i], pattern)
	if df2.severity[i] == "medium":
		medium_alert_time.append(t)
		medium_alert_count.append(df2.alert_count[i])

fig, ax1 = plt.subplots()
ax1.plot(traffic_time, traffic_count, label='Traffic from ' + attacker + " (Netherlands)")

ax2 = ax1.twinx()
ax2.scatter(medium_alert_time, medium_alert_count, c='C1', label='SCAN: Host Sweep')
lines1, labels1 = ax1.get_legend_handles_labels()
lines2, labels2 = ax2.get_legend_handles_labels()
ax1.legend(lines1 + lines2, labels1 + labels2, loc='upper center', fontsize='x-large', shadow=True, bbox_to_anchor=(0.5, 1.25))
ax1.set_xlabel('Time',size='x-large')
ax1.set_ylabel('Num. Incoming DNS Queries/10s', size='x-large', color='C0')
ax2.set_ylabel('Alerts', size='x-large', color='C3')
ax1.set_xlim(['2018-05-01 00:00:00', '2018-05-02 00:00:00'])
ax1.set_ylim(bottom=0)
ax2.set_ylim(bottom=0, top=1.2)
ax2.set_yticks([0, 1])
ax2.set_yticklabels(['False', 'True'])
ax1.tick_params(labelsize=12)
ax2.tick_params(labelsize=12)
xfmt = mdates.DateFormatter('%-I%p')
ax1.xaxis.set_major_formatter(xfmt)
ax1.grid()
fig.savefig(directory + '/alerts_' + attacker + '.pdf', bbox_inches='tight')
fig.tight_layout()
plt.show()