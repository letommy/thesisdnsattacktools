#!/usr/bin/python

import sys
import datetime
import collections
import dpkt
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates

start_time_csv = None
start_time_pcap = None

csv_count = collections.OrderedDict()
pcap_count = collections.OrderedDict()

df = pd.read_csv(sys.argv[1], header=None, usecols=[1], names=['time'])

for time in df.time:
    if start_time_csv == None:
        start_time_csv = time 
    if time > start_time_csv:
        time = datetime.datetime.utcfromtimestamp(float(time + 36000))
        try:
            csv_count[time] += 1
        except KeyError:
            csv_count[time] = 1

pcap_file = open(sys.argv[2])
pcap = dpkt.pcap.Reader(pcap_file)

for ts, buf in pcap:
    if start_time_pcap == None:
        start_time_pcap = ts
        diff = start_time_pcap - start_time_csv
    ts -= diff
    if ts > start_time_csv:
        time = datetime.datetime.utcfromtimestamp(float(int(ts) + 36000))
        try:
            pcap_count[time] += 1
        except KeyError:
            pcap_count[time] = 1

fig, ax = plt.subplots()
ax.plot(csv_count.keys(), csv_count.values(), label='Real Traffic', linewidth=2)
ax.plot(pcap_count.keys(), pcap_count.values(), label='Replayed Traffic', linewidth=2, c='C2')
ax.legend(loc='upper right', fontsize='x-large', shadow=True)
ax.set_xlabel('Time',size='x-large')
ax.set_xlim(['2018-05-01 00:00:00', '2018-05-02 00:00:00'])
ax.set_ylabel('Num. Incoming DNS Queries/s', size='x-large')
ax.set_ylim([0, 1600])
ax.tick_params(labelsize=12)
xfmt = mdates.DateFormatter('%-I%p')
ax.xaxis.set_major_formatter(xfmt)
ax.grid()
fig.savefig('01-05.pdf', bbox_inches='tight')
plt.show()
