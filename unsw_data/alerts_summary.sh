#!/bin/bash

start_time_csv=1525096800
start_time_pcap=1537838350
diff=$(( $start_time_pcap - $start_time_csv ))

date=`echo "$1" | sed 's/^UNSW-\([0-9]\+-[0-9]\+\).*$/\1/'`

IFS=$"\n"
alerts=( $(egrep "$2" "$1" | cut -d, -f 33 | sort | uniq) )

for alert in "${alerts[@]}"
do
	alert_mod=`echo "$alert" | sed 's/(/\\\(/' | sed 's/)/\\\)/'`
	line=`egrep "$2" "$1" | egrep "$alert_mod" | head -n 1`
	severity=`echo "$line" | cut -d, -f 35`
	count=`egrep "$2" "$1" | egrep "$alert_mod" | wc -l`
	start=`egrep "$2" "$1" | egrep "$alert_mod" | head -n 1 | cut -d, -f 2`
	start_epoch=`date --date="$start" +%s`
	start_epoch=$(( $start_epoch - $start_time_pcap + $start_time_csv ))
	start=`date -d @"$start_epoch" '+%Y-%m-%d %H:%M:%S'`
	end=`egrep "$2" "$1" | egrep "$alert_mod" | tail -n 1 | cut -d, -f 2`
	end_epoch=`date --date="$end" +%s`
	end_epoch=$(( $end_epoch - $start_time_pcap + $start_time_csv ))
	end=`date -d @"$end_epoch" '+%Y-%m-%d %H:%M:%S'`
	echo "$date,$2,$alert,$severity,$count,$start,$end"
done